<?php

/**
 * Implementation of hook_contentblocker_data().
 */
function user_contentblocker_data() {
  return array(
    'user' => array(
      'type_title' => t('Users'),
      'title_table' => 'users',
      'title_table_alias' => 'u',
      'title_field' => 'name',
      'title_id_field' => 'uid',
      'title_path_prefix' => 'user',
      'block_table' => 'users',
      'block_table_alias' => 'u',
      'block_field' => 'uid',
      'primary_table' => 'users',
      'primary_table_alias' => 'u',
      'primary_id_field' => 'uid',
      'primary_join_field' => 'uid',
      'object_table' => 'users',
      'object_table_alias' => 'u',
      'object_field' => 'uid',
    ),
    'user_node' => array(
      'save_type' => 'user',
      'type_title' => t('Users'),
      'title_table' => 'users',
      'title_table_alias' => 'u',
      'title_field' => 'name',
      'title_id_field' => 'uid',
      'title_path_prefix' => 'user',
      'block_table' => 'node',
      'block_table_alias' => 'n',
      'block_field' => 'uid',
      'primary_table' => 'users',
      'primary_table_alias' => 'u',
      'primary_id_field' => 'uid',
      'primary_join_field' => 'uid',
      'object_table' => 'node',
      'object_table_alias' => 'n',
      'object_field' => 'uid',
    ),
    'user_comment' => array(
      'save_type' => 'user_node',
      'type_title' => t('Users'),
      'title_table' => 'users',
      'title_table_alias' => 'u',
      'title_field' => 'name',
      'title_id_field' => 'uid',
      'title_path_prefix' => 'user',
      'block_table' => 'comments',
      'block_table_alias' => 'c',
      'block_field' => 'uid',
      'primary_table' => 'users',
      'primary_table_alias' => 'u',
      'primary_id_field' => 'uid',
      'primary_join_field' => 'uid',
      'object_table' => 'comments',
      'object_table_alias' => 'c',
      'object_field' => 'uid',
    ),
  );
}

/**
 * Implementation of hook_contentblocker_options().
 */
function user_contentblocker_options($object_type, $object) {
  global $user;

  switch ($object_type) {
    case 'node':
    case 'comment':
    case 'user':
      $return = array();
      // Don't allow block of user's own content.
      if ($object->uid != $user->uid && isset($object->name) && !empty($object->name)) {
        $return['user_'. $object_type] = array(
          t('Block content by @name', array('@name' => $object->name)),
          t('Unblock content by @name', array('@name' => $object->name)),
        );
      }
      return $return;
  }
}

