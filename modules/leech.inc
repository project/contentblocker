<?php

/**
 * Implementation of hook_contentblocker_data().
 */
function leech_contentblocker_data() {
  return array(
    'leech_news_feed' => array(
      // Data for use in contentblocker/ pages.
      // Title for menu item.
      'type_title' => t('Leech feeds'),
      // Table that item title should be drawn from.
      'title_table' => 'node',
      // Alias for this table.
      'title_table_alias' => 'n',
      // Field in title table that should be used for the title.
      'title_field' => 'title',
      // Id field that links to the id field in contentblocker.
      'title_id_field' => 'nid',
      // Path prefix for links to items, e.g., for node/21 this is 'node'.
      'title_path_prefix' => 'node',
      // Data for use in blocking content.
      // Table that should be joined on for blocking.
      'block_table' => 'leech_news_item',
      // Alias for blocking table.
      'block_table_alias' => 'lni',
      // Field used for blocking.
      'block_field' => 'fid',
      // Primary table where blocked item resides.
      'primary_table' => 'leech_news_feed',
      'primary_table_alias' => 'lnf',
      'primary_id_field' => 'nid',
      'primary_join_field' => 'nid',
      'object_table' => 'node',
      'object_table_alias' => 'n',
      'object_field' => 'nid',
    ),
    'leech_news_item' => array(
      'save_type' => 'leech_news_feed',
      'type_title' => t('Leech feed items'),
      'title_table' => 'node',
      'title_table_alias' => 'n',
      'title_field' => 'title',
      'title_id_field' => 'nid',
      'title_path_prefix' => 'node',
      'block_table' => 'leech_news_item',
      'block_table_alias' => 'lni',
      'block_field' => 'fid',
      // Primary table where blocked item resides.
      'primary_table' => 'leech_news_item',
      'primary_table_alias' => 'lni',
      'primary_id_field' => 'fid',
      'primary_join_field' => 'nid',
      'object_table' => 'node',
      'object_table_alias' => 'n',
      'object_field' => 'nid',
    ),
  );
}

/**
 * Implementation of hook_contentblocker_options().
 */
function leech_contentblocker_options($object_type, $object) {
  switch ($object_type) {
    case 'node':
      $return = array();
      // Both items and feeds have leech_news set.
      // Link for feeds.
      if (isset($object->leech_news) && !isset($object->leech_news_item)) {
        $return['leech_news_feed'] = array(
          t('Block content from this feed'),
          t('Unblock content from this feed'),
        );
      }
      // Link for feeds.
      elseif (isset($object->leech_news_item)) {
        $return['leech_news_item'] = array(
          t('Block content from this feed'),
          t('Unblock content from this feed'),
        );
      }
      return $return;
  }
}
