Content Blocker:
*****************

This module enables users to block the content they see according to various
parameters. Current implementations are for blocking by user and blocking
by leech feed.

Logged in users with appropriate permissions see links below selected
content, e.g., 'Block content by userx'. Clicking this link triggers
an AJAX update (or loads a form, if Javascript isn't available). On
subsequent page loads, any content by the blocked user will be filtered out
of node lists, views, etc.

Depends on Fast Toggle module.

Developer usage:

Content Blocker is fully modularized so as to be easily extended. New types
of blocking options can be added by writing an include file and adding it
to the module's /modules directory. The .inc file will be automatically
detected and offered as a choice for blocking if the module it implements
is available on the site.

Blocking comments

To have comments as well as nodes blocked for a given user, include the 
following function in your theme's template.php file (if it's a PHPTemplate
theme; otherwise, adapt it for your theme):

<?php
function phptemplate_comment_view($comment, $links = array(), $visible = 1) {
  global $user;

  // Render the comment only if the comment's author isn't blocked by the current user.
  if (!$user->uid || ($user->uid && !db_num_rows(db_query("SELECT id FROM {contentblocker} WHERE uid = %d AND id = %d AND type = 'user'", $user->uid, $comment->uid)))) {
    return theme_comment_view($comment, $links, $visible);
  }
  return '';
}
?>
